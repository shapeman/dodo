import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import java.io.StringReader;

public class CEventConsumer {
    CEventConsumer() {
        this.TransferMsgToKafka();
    }

    public void TransferMsgToKafka() {
        // consume msg from rabbit mq
        ReceiveMQ mqConsumer = new ReceiveMQ();

        mqConsumer.addConsumerHandler(this::handleMessage);

        try {
            mqConsumer.consumeMessage();
        } catch (Exception e) {
            e.printStackTrace();
        }


        // push to kafka
    }

    private void handleMessage(String message) {
        // handle messages;
        System.out.println("Hello");
        System.out.println(message);

        Document doc = convertStringToXMLDocument( message );

        NodeList nodeList = doc.getElementsByTagName("pv");

        Node node = nodeList.item(0);

        String devType = node.getAttributes().getNamedItem("devtype").getNodeValue();

        String bsType = node.getAttributes().getNamedItem("bstype").getNodeValue();

        String devId = node.getAttributes().getNamedItem("devid").getNodeValue();

        if(devType.equals("4")) {

            if(bsType.equals("9")) {
                // Lat Long Notification
                DeviceInfo device = CWebSocketInfo.Acquire().getDeviceMetaData(devId);

                Publish.publishMessage(device.toString());
            }

            if(bsType.equals("12")) {
                // SOS Alarm
                DeviceInfo device = CWebSocketInfo.Acquire().getDeviceMetaData(devId);
                Publish.publishMessage(device.toString());

            }

            if(bsType.equals("10")) {
                // Online Status
                System.out.println("To be discussed -- Online Status");
            }

        } else {
            // to be discussed
            System.out.println("to be discussed");
        }
    }


    private static Document convertStringToXMLDocument(String xmlString) {
        //Parser that produces DOM object trees from XML content
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();

        //API to obtain DOM Document instance
        DocumentBuilder builder = null;
        try
        {
            //Create DocumentBuilder with default configuration
            builder = factory.newDocumentBuilder();

            //Parse the content to Document object
            Document doc = builder.parse(new InputSource(new StringReader(xmlString)));
            return doc;
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
        return null;
    }
}
