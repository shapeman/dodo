import java.net.URI;
import java.net.URISyntaxException;

class CEventHandler {
    private String webSocketAddress;


    CEventHandler() {
        this.EstablishConnection();
    }

    void EstablishConnection() {

            // Get WebSocket Address

            CWebSocketInfo webSocketInfo = CWebSocketInfo.Acquire();

            this.webSocketAddress = webSocketInfo.getWebsocketAddress();

            while (this.webSocketAddress == null) {
                this.webSocketAddress = webSocketInfo.getWebsocketAddress();
                // QP6
                try {
                    Thread.sleep(10000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
            System.out.println("CEventHandler");
            System.out.println(this.webSocketAddress);


        Client webSocketClient = null;
        try {
            webSocketClient = new Client(new URI(this.webSocketAddress));
        } catch (URISyntaxException e) {
            e.printStackTrace();
        }


        webSocketClient.addMessageHandler(
                    message -> this.handleMessage(message)
            );

            // connect to websocket server;
        try {
            webSocketClient.connectBlocking();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        // send message to websocket server
        webSocketClient.send("<msg msgid='59' index='0' ><points all='1'></points></msg>");

    }

    private void handleMessage(String message) {
        SendMQ.sendMessage(message);
    }

}

