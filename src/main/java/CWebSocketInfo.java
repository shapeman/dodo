import com.squareup.okhttp.*;
import kong.unirest.json.JSONArray;
import kong.unirest.json.JSONObject;

import java.io.IOException;
import java.util.Iterator;
import java.util.concurrent.ConcurrentHashMap;
import java.util.logging.Logger;

class DeviceInfo {
    public String name;
    public String policeid;
    public String areaid;
    public String videoip;
    public String videoport;
    public String sid;
    public String devpoint;
    public String devid;
    public String sn;
    public String type;
    public String subtype;

    @Override
    public String toString() {
        
        return "DeviceInfo{" +
                "name='" + name + '\'' +
                ", policeid='" + policeid + '\'' +
                ", areaid='" + areaid + '\'' +
                ", videoip='" + videoip + '\'' +
                ", videoport='" + videoport + '\'' +
                ", sid='" + sid + '\'' +
                ", devpoint='" + devpoint + '\'' +
                ", devid='" + devid + '\'' +
                ", sn='" + sn + '\'' +
                ", type='" + type + '\'' +
                ", subtype='" + subtype + '\'' +
                '}';
    }
}

class CWebSocketInfo {

    private String websocketAddress;

    // concurrent hashmap to avoid mutex
    // QP2
    private final ConcurrentHashMap<String, DeviceInfo> deviceMetaData = new ConcurrentHashMap<>();

    public DeviceInfo getDeviceMetaData(String policeid) {
        return this.deviceMetaData.get(policeid);
    }

    public String getWebsocketAddress() {
        return websocketAddress;
    }

    public void setWebsocketAddress(String websocketAddress) {
        this.websocketAddress = websocketAddress;
    }


    private static CWebSocketInfo cWebSocketInfo;

    // getInstance
    public static CWebSocketInfo Acquire() {
        if(cWebSocketInfo == null) {
            cWebSocketInfo = new CWebSocketInfo();
        }

        return cWebSocketInfo;
    }

    void GetArchInfo() throws IOException, InterruptedException {

        String APPKEY = ConfigReader.Acquire().getProperty("APPKEY");
        String APPSECRET = ConfigReader.Acquire().getProperty("APPSECRET");
        String USER = ConfigReader.Acquire().getProperty("USER");
        String PASSWORD = ConfigReader.Acquire().getProperty("PASSWORD");

        while (true) {
            OkHttpClient client = new OkHttpClient();

            // change from here
            MediaType mediaType = MediaType.parse("text/plain");
            //RequestBody requestBody = RequestBody.create(mediaType, "");

            // --> construct body
            RequestBody requestBody = RequestBody.create(mediaType, "appkey="+APPKEY+"&appsecret="+APPSECRET+"&user="+USER+"&password="+PASSWORD);
            // till here

            Request request = new Request.Builder()
                    .url(ConfigReader.Acquire().getProperty("URL"))
                    .method("POST", requestBody)
                    .build();

            Response response = null;

            try {
                response = client.newCall(request).execute();
            } catch (IOException e) {
                e.printStackTrace();
            }
            {
                assert response != null;
                if(response.isSuccessful()) {
                    ResponseBody body = response.body();

                    // create a json object here from kong.unirest.json.JSONObject; -> this is parsing
                    // string -> JSONObject -> we can get data from this
                    JSONObject data = new JSONObject(body.string());

                    // QP1
                    assert data != null;

                    String webSocketAddr = data.getJSONObject("data").getJSONObject("stwebsvr").getString("url");


                    this.setWebsocketAddress(webSocketAddr);


                    JSONArray userList = data.getJSONObject("data").getJSONArray("userList");

                    Iterator userListIterator = userList.iterator();

                    while (userListIterator.hasNext()) {
                        // get the object
                        JSONObject obj = (JSONObject) userListIterator.next();

                        DeviceInfo device = new DeviceInfo();

                        device.areaid = obj.getString("areaid");

                        device.policeid = obj.getString("policeid");

                        device.devid = obj.getString("devid");

                        device.devpoint = obj.getString("devpoint");

                        device.sid = obj.getString("sid");

                        device.name = obj.getString("name");

                        device.videoip = obj.getString("videoip");

                        device.videoport = obj.getString("videoport");

                        device.sn = obj.getString("sn");

                        device.type = obj.getString("type");

                        device.subtype = obj.getString("subtype");

                        if(this.deviceMetaData.containsKey(device.policeid)) {
                            this.deviceMetaData.replace(device.policeid, device);
                        } else {
                            this.deviceMetaData.put(device.policeid, device);
                        }


                    }

                } else {
                    Logger.getAnonymousLogger().fine("Response is not successful.");
                    Thread.sleep(30000);

                }
        }

        }
    }
}
