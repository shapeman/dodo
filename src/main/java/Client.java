import org.java_websocket.client.WebSocketClient;
import org.java_websocket.handshake.ServerHandshake;

import java.net.URI;
import java.util.Date;

public class Client extends WebSocketClient {
    Date openedTime,closedTime;

    private MessageHandler messageHandler;



    public Client(URI serverUri) {
        super(serverUri);
    }


    @Override
    public void onOpen(ServerHandshake handshakedata) {
        openedTime=new Date();
        System.out.println("Opened Connection "+this.getURI());
    }

    @Override
    public void onMessage(String message) {
        if (this.messageHandler != null) {
            this.messageHandler.handleMessage(message);
        }
    }

    @Override
    public void onClose(int code, String reason, boolean remote) {
        int timeElapsed = connectionAliveTime();
        System.out.println("Connection time (seconds) "+timeElapsed);
    }

    @Override
    public void onError(Exception ex) {
        System.out.println("Error in connection "+ex.getMessage());
    }

    public int connectionAliveTime(){
        closedTime=new Date();
        return (int) (closedTime.getTime()-openedTime.getTime())/1000;
    }
    public void addMessageHandler(Client.MessageHandler msgHandler) {
        this.messageHandler = msgHandler;
    }

    public interface MessageHandler {

        void handleMessage(String message);
    }
}