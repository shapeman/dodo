import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

public class ConfigReader {

    Properties config;

    private static ConfigReader configReader;

    ConfigReader() {
        this.config = new Properties();

        try {
            FileInputStream configFile = new FileInputStream("src/main/resources/config.properties");

            config.load(configFile);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static ConfigReader Acquire() {
        if(configReader == null) {
            configReader = new ConfigReader();
        }

        return  configReader;
    }

    String getProperty( String key) {
        return this.config.getProperty(key);
    }


}


