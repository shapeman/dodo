import java.io.IOException;

public class Dodo {

    public static void main(String[] args) {

        CWebSocketInfo webSocketInfo = CWebSocketInfo.Acquire();

        // constructor for singleton class
        Thread t1 = new Thread(() -> {
            try {
                webSocketInfo.GetArchInfo();
            } catch (IOException | InterruptedException e) {
                e.printStackTrace();
            }
        });

        Thread t2 = new Thread(() -> {
            CEventHandler eventHandler = new CEventHandler();
        });

        t1.start();
        t2.start();

        CEventConsumer eventConsumer = new CEventConsumer();

    }

}
