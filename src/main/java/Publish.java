import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.Producer;
import org.apache.kafka.clients.producer.ProducerRecord;

import java.util.Properties;

public class Publish {
    public static String topic = "test";
    public static void publishMessage(String value){

        Properties props = new Properties();
        props.put("bootstrap.servers", "172.30.71.4:9092");
        props.put("key.serializer", "org.apache.kafka.common.serialization.StringSerializer");
        props.put("value.serializer", "org.apache.kafka.common.serialization.StringSerializer");
        props.put("retries", 0);
        props.put("batch.size", 16384);
        props.put("linger.ms", 1);
        props.put("acks", "all");
        props.put("buffer.memory", "12582912");
        props.put("connections.max.idle.ms", "300000");


        Producer<String, String> producer = new KafkaProducer<>(props);

        producer.send(new ProducerRecord<>(topic, value));


        producer.close();


    }


}
