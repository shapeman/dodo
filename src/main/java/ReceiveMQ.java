import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;
import com.rabbitmq.client.DeliverCallback;

import java.nio.charset.StandardCharsets;

public class ReceiveMQ {
    private final static String QUEUE_NAME = "test";
    static int i=0;

    private RabbitMqConsumeMsg rabbitMqConsumeMsg;

    public void consumeMessage() throws Exception{

        ConnectionFactory factory = new ConnectionFactory();

        factory.setHost("localhost");

        factory.setPort(15672);

        factory.setUsername("myuser");

        factory.setPassword("mypassword");

        Connection connection = factory.newConnection();

        Channel channel = connection.createChannel();

        channel.queueDeclare(QUEUE_NAME, true, false, false, null);
        System.out.println(" [*] Waiting for messages. To exit press CTRL+C");

        DeliverCallback deliverCallback = (consumerTag, delivery) -> {
            String message = new String(delivery.getBody(), StandardCharsets.UTF_8);
            System.out.println(" [x] Received '" + message + "'");
            long deliverytag = delivery.getEnvelope().getDeliveryTag();
            //channel.basicAck(deliverytag, false);
            if(i%2==0) {
                channel.basicReject(deliverytag, true);
                System.out.println(" [x] rejected '" + message + "'");
            }
            else {
                channel.basicAck(deliverytag, true);

                if(this.rabbitMqConsumeMsg != null) {
                    this.rabbitMqConsumeMsg.consumeRabbitMqMsg(message);
                }

                System.out.println(" [x] accepted '" + message + "'");
            }
            i++;
            //return;
        };
        channel.basicConsume(QUEUE_NAME, false, deliverCallback, consumerTag -> { });
    }

    public void addConsumerHandler(ReceiveMQ.RabbitMqConsumeMsg consumer) {
        this.rabbitMqConsumeMsg = consumer;
    }


    public interface RabbitMqConsumeMsg {
        void consumeRabbitMqMsg(String message);
    }



}
