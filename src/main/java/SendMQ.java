import com.rabbitmq.client.AMQP;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.concurrent.TimeoutException;

public class SendMQ {
    // change this queue name in production
    private final static String QUEUE_NAME = "test";


    public static void sendMessage(String message) {
        ConnectionFactory factory = new ConnectionFactory();

        factory.setHost("localhost");

        factory.setPort(15672);

        factory.setUsername("myuser");

        factory.setPassword("mypassword");

        try (Connection connection = factory.newConnection();
             Channel channel = connection.createChannel()) {
            channel.queueDeclare(QUEUE_NAME, true, false, false, null);

            channel.basicPublish("", QUEUE_NAME, new AMQP.BasicProperties.Builder()
                    .contentType("text/plain")
                    .deliveryMode(2)
                    .priority(1)
                    .userId("guest")
                    .build(), message.getBytes(StandardCharsets.UTF_8));
            System.out.println(" [x] Sent '" + message + "'");
        } catch (IOException e) {
            e.printStackTrace();
        } catch (TimeoutException e) {
            e.printStackTrace();
        }
    }
}
